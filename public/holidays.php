
/**
 * Created by PhpStorm.
 * User: jorrit
 * Date: 24-3-16
 * Time: 0:02
 */

<html xmlns="http://www.w3.org/1999/html">
<head>
    <script src="js/jquery-2.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class='main'>
    <div class="container-fluid">
        <div class="row">
            <div class="content col-md-12">
                <ul class="nav nav-pills-stacked">
                    <li role="presentation" class="active"><a href="index.php">terug</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class='container'>
        <div class='row'>
            <div class="col-md-12">
                <div class="content form form-ill">
                    <div class="page-header">
                        <h3>Feestdagen invoeren/h3>
                    </div>
                    <form>
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">Feestdag naam</label>
                            <input type="text" class="form-control" name="holiday">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Startdatum</label>
                            <input type="date" class="form-control" name="startdate" >
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Einddatum</label>
                            <input type="date" class="form-control" name="enddate" >
                        </fieldset>
                        <button type="submit" class="btn btn-primary">Doorsturen</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    </hr>
    <div class='container'>
        <div class='row'>
            <div class="col-md-12">
                <div class="content form form-ill">
                    <div class="page-header">
                        <h3>Feestdagen werwijderen</h3>
                    </div>
                    <form>
                        <fieldset class="form-group">
                            <label for="exampleSelect1">Selecteer feestdag</label>
                            <select class="form-control" name="select-holiday" id="project-select">
                                <option value="1" >kerst</option>
                                <option value="2" >pasen</option>
                                <option value="3" >eigen feestdag</option>
                            </select>
                        </fieldset>
                        <button type="submit" class="btn btn-primary">Doorsturen</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    </hr>
    <div class='container'>
        <div class='row'>
            <div class="col-md-12">
                <div class="content form form-ill">
                    <div class="page-header">
                        <h3>Feestdagen wijzigen</h3>
                    </div>
                    <form>
                        <fieldset class="form-group">
                            <label for="exampleSelect1">Selecteer feestdag</label>
                            <select class="form-control" name="select-holiday" id="project-select">
                                <option value="1" >kerst</option>
                                <option value="2" >pasen</option>
                                <option value="3" >eigen feestdag</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Startdatum</label>
                            <input type="date" class="form-control" name="startdate" >
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Einddatum</label>
                            <input type="date" class="form-control" name="enddate" >
                        </fieldset>
                        <button type="submit" class="btn btn-primary">Doorsturen</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>
</body>
</html>