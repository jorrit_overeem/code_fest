
/**
 * Created by PhpStorm.
 * User: jorrit
 * Date: 23-3-16
 * Time: 23:40
 */

<html>
<head>
    <script src="js/jquery-2.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class='main'>
    <div class="container-fluid">
        <div class="row">
            <div class="content col-md-12">
                <ul class="nav nav-pills-stacked">
                    <li role="presentation" class="active"><a href="index.php">terug</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class='container'>
        <div class='row'>
            <div class="col-md-12">
                <div class="content form form-ill">
                    <div class="page-header">
                        <h3>Medewerker registratie</h3>
                    </div>
                    <form>
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">Voorletter</label>
                            <input type="text" class="form-control" name="prefix">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Voornaam</label>
                            <input type="date" class="form-control" name="firstname" >
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="employee">Tussenvoegsel</label>
                            <input type="text" class="form-control" name="suffix">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">Achternaam</label>
                            <input type="text" class="form-control" name="lastname" >
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Email</label>
                            <input type="text" class="form-control" name="geboortedatum" >
                            </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleSelect1">Afdeling</label>
                            <select class="form-control" id="project-select">
                                <option value="1" >administratie</option>
                                <option value="2" >medewerker</option>
                                <option value="3" >manager</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleSelect1">Permissie</label>
                            <select class="form-control" id="project-select">
                                <option value="admin" >administratie</option>
                                <option value="werknemer" >medewerker</option>
                                <option value="manager" >manager</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="hoursworked">Contract uren</label>
                            <input type="text" class="form-control" id="lastname">
                            <label for="overhours">Deeltijfactor</label>
                            <input type="text" class="form-control" id="firstname">
                        </fieldset>
                        <button type="submit" class="btn btn-primary">Doorsturen</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>
</body>
</html>