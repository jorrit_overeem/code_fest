<html>
<head>
    <script src="js/jquery-2.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="main">
    <div class="container-fluid">
        <div class="row">
            <div class="content col-md-12">
                <ul class="nav nav-pills-stacked">
                    <li role="presentation" class="active"><a href="index.php">terug</a></li>
                    <li role="presentation" class="active"><a href="employeereg.php">registreer nieuwe medewerker</a></li>
                    <li role="presentation" class="active"><a href="index.php">feestdagen</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <h2>Adminsitratie</h2>
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td>Deeltijdfactor</td>
                    <td><input type="text" name="parttime_factor" placeholder="in te vullen door medewerker" /></td>
                </tr>
                <tr>
                    <td>Aantal dagen voor invoer vrije dag</td>
                    <td><input type="text" name="number_of_days" placeholder="in te vullen door medewerker" /></td>
                </tr>
                <tr>
                    <td>Deeltijdfactor vakantie dagen</td>
                    <td><input type="text" name="factor_holidays" placeholder="in te vullen door medewerker" /></td>
                </tr>
                <tr>
                    <td>Prijs inkoop vrije dag</td>
                    <td><input type="text" name="price_dayoff" placeholder="in te vullen door medewerker" /></td>
                </tr>
                </tbody>
            </table>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>afdeling</th>
                    <th></th>
                    <th>minimum bezetting</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>helpdesk</td>
                    <td></td>
                    <td>26</td>
                </tr>
                <tr>
                    <td>commercieel</td>
                    <td></td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>administratief</td>
                    <td></td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>directie</td>
                    <td></td>
                    <td>2</td>
                </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-primary">Doorsturen</button>
        </div>
    </div>
</div>


</body>
</html>