<html>
<head>
    <script src="js/jquery-2.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
</head>
<body>
<div class='header'>
    <div class='container-fluid'>
        <div class='row'>
            <div class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                            <li><a href="manager.php">Manager</a></li>
                            <li><a href="aministrative_employee.php">Administrative</a></li>
                            <li><a href="#">Link</a></li>
                            <li><a href="#">Link</a></li>
                        </ul>
                        <div class="nav navbar-nav navbar-right">
                            <a class="navbar-brand" href="#"><span class='glyphicon glyphicon-user user-icon'></span></a>
                        </div>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </div>
        </div>
    </div>
</div>
<div class='main'>
    <div class="container-fluid">
        <div class="row">
            <div class="module">
                <ul class="nav nav-pills">
                    <li role="presentation" class="active"><a href="#">ziek</a></li>
                    <li role="presentation"><a href="#">vakantie</a></li>
                    <li role="presentation"><a href="#">uren</a></li>
                    <li role="presentation"><a href="#">overzicht</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class='container'>
        <div class='row'>
            <div class="col-md-12">
                <div class="content form form-ill">
                    <div class="page-header">
                        <h3>Ziek</h3>
                    </div>
                    <form>
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">Van</label>
                            <input data-provide="datepicker" data-date-format="mm/dd/yyyy" type="text" class="form-control " id='datetimepicker' />
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Tot</label>
                            <input type="date" class="form-control" id="exampleInputPassword1">
                            </fieldset>
                        <button type="submit" class="btn btn-primary">Doorsturen</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class='container'>
        <div class='row'>
            <div class="col-md-12">
                <div class="content form form-holiday">
                    <div class="page-header">
                        <h3>Vakantie</h3>
                    </div>
                    <form>
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">Van</label>
                            <input type="date" class="form-control" id="exampleInputEmail1">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Tot</label>
                            <input type="date" class="form-control" id="exampleInputPassword1">
                            </fieldset>
                            <button type="submit" class="btn btn-primary">Doorsturen</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div class='container'>
        <div class='row'>
            <div class="col-md-12">
                <div class="content form form-holiday">
                    <div class="page-header">
                        <h3>Uren</h3>
                    </div>
                    <form>
                        <fieldset class="form-group">
                            <label for="employee">Werknemer</label>
                            <div class="input">
                                <input type="text" class="form-control employee-first" id="firstname">
                            </div>
                            <div class="input">
                                <input type="text" class="form-control employee-name" id="lastname">
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleSelect1">Project</label>
                            <select class="form-control" id="project-select">
                                <option>Project 1</option>
                                <option>Project 2</option>
                                <option>Project 3</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="hoursworked">Aantal uren</label>
                            <input type="text" class="form-control" id="lastname">
                            <label for="overhours">Over uren</label>
                            <input type="text" class="form-control" id="firstname">
                        </fieldset>
                            <button type="submit" class="btn btn-primary">Doorsturen</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div class='container'>
        <div class='row'>
            <div class="col-md-12">
                <div class="content form form-holiday">
                    <div class="page-header">
                        <h3>Overzicht</h3>
                    </div>
                    <form>
                        <fieldset class="form-group">
                            <label for="employee">Werknemer</label>
                            <div class="input">
                                <input type="text" class="form-control employee-first" id="firstname">
                            </div>
                            <div class="input">
                                <input type="text" class="form-control employee-name" id="lastname">
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">Van</label>
                            <input type="date" class="form-control" id="exampleInputEmail1">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Tot</label>
                            <input type="date" class="form-control" id="exampleInputPassword1">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="hoursworked">gewerkt</label>
                            <input type="text" class="form-control" id="hoursworked">
                            <label for="overhours">overuren</label>
                            <input type="text" class="form-control" id="overhours">
                            <label for="holiday">vakantie</label>
                            <input type="text" class="form-control" id="holiday">
                            <label for="ill">ziek</label>
                            <input type="text" class="form-control" id="ill">

                        </fieldset>
                        <button type="submit" class="btn btn-primary">Doorsturen</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>
</body>
</html>