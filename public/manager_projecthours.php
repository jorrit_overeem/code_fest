<html>
<head>
    <script src="js/jquery-2.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="main">
    <div class="container-fluid">
        <div class="row">
            <div class="content col-md-12">
                <ul class="nav nav-pills-stacked">
                    <li role="presentation" class="active"><a href="manager.php">terug</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <h2>Wijzigen project uren</h2>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Projectnaam</th>
                    <th>Aantal uur vast</th>
                    <th>Aantal uur gemaakt</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Loop</td>
                    <td>16</td>
                    <td>26</td>
                </tr>
                <tr>
                    <td>Mary</td>
                    <td>2316</td>
                    <td>2516</td>
                </tr>
                <tr>
                    <td>July</td>
                    <td>23</td>
                    <td>25</td>
                </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-primary">Doorsturen</button>
        </div>
    </div>
</div>


</body>
</html>